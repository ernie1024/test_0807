<style>
    nav .navbar-nav > li.nav-item.active {
        background-color: #f9ff84;
    }
</style>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="product_list.php">商品</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="cart.php">購物車 <span id="qty-badge" class="badge badge-primary"></span>
</a>
            </li>

        </ul>
        <ul class="navbar-nav">
            <?php if(! isset($_SESSION['user'])): //如果沒有設定 ?> 
                <li class="nav-item <?= $pageName=='login' ? 'active' : '' ?>">
                    <a class="nav-link" href="login.php">登入</a>
                </li>
                <li class="nav-item <?= $pageName=='register' ? 'active' : '' ?>">
                    <a class="nav-link" href="register.php">註冊</a>
                </li>
            <?php else: ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?= $_SESSION['user']['nickname'] ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">編輯個人資料</a>
                        <a class="dropdown-item" href="history.php">訂單查詢</a>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="logout.php">登出</a>
                </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>

<script>
    var changeQty = function(obj){
        var total = 0;
        for(var s in obj){
            total += obj[s];
        }
        $('#qty-badge').text(total);
    };

    $.get('add_to_cart.php', function(data){
        changeQty(data);
    }, 'json');
</script>