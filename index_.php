<?php include __DIR__. '/__html_head.php' ?>

<div class="container">
    <?php include __DIR__. '/__navbar.php' ?>

<style>
form>.form-group>small{
    color:red !important;
    display:none;
}
</style>
<div class="row justify-content-md-center" style="margin-top: 20px" >

<div class="col-md-6">
    <div class="card">
        <div class="card-body">
            <div class="card-title">會員註冊</div>

            <form name="form1" method="post">
                <div class="form-group">
                    <label for="nickname">暱稱 *</label>
                    <input type="text" class="form-control" id="nickname" name="nickname" placeholder="暱稱">
                    <small id="nameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="email">電子郵件 * (當作登入帳號)</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="電子郵件">
                    <small id="mobileHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>

                <div class="form-group">
                    <label for="password">密碼 *</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="密碼">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>

                <div class="form-group">
                    <label for="mobile">手機號碼 *</label>
                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="手機號碼">
                    <small id="birthdayHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>

                <div class="form-group">
                    <label for="birthday">生日</label>
                    <input type="text" class="form-control" id="birthday" name="birthday" placeholder="生日">
                    <small id="birthdayHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>

                <div class="form-group">
                    <label for="address">地址</label>
                    <textarea class="form-control" name="address" id="address" cols="30" rows="3" placeholder="請填寫地址"></textarea>
                    <small id="addressHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>

                <button type="submit" class="btn btn-primary">註冊會員</button>
            </form>
        </div>


    </div>
</div>


</div>




</div>

</div>
<?php include __DIR__. '/__html_foot.php' ?>